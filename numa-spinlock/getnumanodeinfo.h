/* Get the maxinum NUMA node number, Linux version.
   Copyright (C) 2018 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

struct numa_node_info
{
  /* The maximium NUMA node number.  */
  unsigned int max_node;
  /* The number of NUMA nodes.  */
  unsigned int node_count;
};

extern struct numa_node_info __get_numa_node_info (void)
  __attribute__ ((visibility ("hidden")));
