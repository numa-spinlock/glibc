/* NUMA spinlock
   Copyright (C) 2018 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <sched.h>
#ifndef HAVE_GETCPU
#include <unistd.h>
#include <syscall.h>
#endif
#include <errno.h>
#include <atomic.h>
#include "numa-spinlock.h"
#include "getnumanodeinfo.h"

#if !defined HAVE_GETCPU && defined _LIBC
# define HAVE_GETCPU
#endif

#ifndef OPTIMIZE_SINGLE_NODE
# define OPTIMIZE_SINGLE_NODE 1
#endif

#ifndef __glibc_unlikely
# define __glibc_unlikely(cond)	__builtin_expect ((cond), 0)
#endif

/* Get the next thread pointed to by *NEXT_P.  NB: We must use a while
   spin loop to load NEXT_P since there is a small window before *NEXT_P
   is updated.  */

static inline struct numa_spinlock_info *
get_numa_spinlock_info_next (struct numa_spinlock_info **next_p)
{
  struct numa_spinlock_info *next;
  while (!(next = atomic_load_relaxed (next_p)))
    atomic_spin_nop ();
  return next;
}

/* While holding the global NUMA spinlock, run the workload of the
   thread pointed to by SELF first, then run the workload for each
   thread on the thread list pointed to by HEAD_P and wake up the
   thread.  */

static inline void
run_numa_spinlock (struct numa_spinlock_info *self,
		   struct numa_spinlock_info **head_p)
{
  struct numa_spinlock_info *next, *current;

  /* Run the SELF's workload. */
  self->result = self->workload (self->argument);

  /* Process workloads for the rest of threads on the thread list.
     NB: The thread list may be prepended by other threads at the
     same time.  */

retry:
   /* If SELF is the first thread of the thread list pointed to by
      HEAD_P, clear the thread list.  */
  current = atomic_compare_and_exchange_val_acq (head_p, NULL, self);
  if (current == self)
    {
      /* Since SELF is the only thread on the list, clear SELF's pending
         field and return.  */
      atomic_store_release (&current->pending, 0);
      return;
    }

  /* CURRENT will have the previous first thread of the thread list
     pointed to by HEAD_P and *HEAD_P will point to SELF.  */
  current = atomic_exchange_acquire (head_p, self);

  /* NB: No need to check if CURRENT == SELF here since SELF can never
     be CURRENT.  */

repeat:
  /* Get the next thread.  */
  next = get_numa_spinlock_info_next (&current->next);

  /* Run the CURRENT's workload and clear CURRENT's pending field. */
  current->result = current->workload (current->argument);
  current->pending = 0;

  /* Process the workload for each thread from CURRENT to SELF on the
     thread list.  Don't pass beyond SELF since SELF is the last thread
     on the list.  */
  if (next == self)
    goto retry;
  current = next;
  goto repeat;
}

/* Apply for the NUMA spinlock, LOCK, with the NUMA spinlock info data
   pointed to by SELF.  */

void
apply_numa_spinlock (struct numa_spinlock *lock,
		     struct numa_spinlock_info *self)
{
  struct numa_spinlock_info *first, *next;
  struct numa_spinlock_info **head_p;

  self->next = NULL;
  /* We want the global NUMA spinlock.  */
  self->pending = 1;
  head_p = &lock->lists[self->node].head;
  /* FIRST will have the previous first thread of the local NUMA spinlock
     kist and *HEAD_P will point to SELF.  */
  first = atomic_exchange_acquire (head_p, self);
  if (first)
    {
      /* SELF has been prepended to the thread list pointed to by
	 HEAD_P.  NB: There is a small window between updating
	 *HEAD_P and self->next.  */
      atomic_store_release (&self->next, first);
      /* Let other threads run first since another thread will run our
	 workload for us.  */
      sched_yield ();
      /* Spin until our PENDING is cleared.  */
      while (atomic_load_relaxed (&self->pending))
	atomic_spin_nop ();
      return;
    }

  /* NB: Now SELF must be the only thread on the thread list pointed
     to by HEAD_P.  Since thread is always prepended to HEAD_P, we
     can use *HEAD_P == SELF to check if SELF is the only thread on
     the thread list.  */

#if OPTIMIZE_SINGLE_NODE
  if (__glibc_unlikely (lock->node_count == 1))
    {
      /* If there is only one node, there is no need for the global
         NUMA spinlock.  */
      run_numa_spinlock (self, head_p);
      return;
    }
#endif

  /* FIRST will have the previous first thread of the local NUMA spinlock
     list of threads which holds the global NUMA spinlock, which will
     point to SELF.  */
  first = atomic_exchange_acquire (&lock->owner, self);
  if (first)
    {
      /* SELF has been prepended to the thread list pointed to by
	 lock->owner.  NB: There is a small window between updating
	 *HEAD_P and first->next.  */
      atomic_store_release (&first->next, self);
      /* Spin until the list of threads which holds the global NUMA
	 spinlock clears our PENDING.  */
      while (atomic_load_relaxed (&self->pending))
	atomic_spin_nop ();
    }

  /* We get the global NUMA spinlock now.  Run our workload.  */
  run_numa_spinlock (self, head_p);

  /* SELF is the only thread on the list if SELF is the first thread
     of the thread list pointed to by lock->owner.  In this case, we
     simply return.  */
  if (!atomic_compare_and_exchange_bool_acq (&lock->owner, NULL, self))
    return;

  /* Wake up the next thread.  */
  next = get_numa_spinlock_info_next (&self->next);
  atomic_store_release (&next->pending, 0);
}

/* Initialize the NUMA spinlock info data pointed to by INFO from a
   pointer to the NUMA spinlock, LOCK.  */

int
init_numa_spinlock_info (struct numa_spinlock *lock,
			 struct numa_spinlock_info *info)
{
  memset (info, 0, sizeof (*info));
  /* If the number of NUMA nodes is 1, use 0 as the NUMA node number.  */
  if (lock->node_count == 1)
    return 0;
  unsigned int node;
#ifdef HAVE_GETCPU
  int err_ret = getcpu (NULL, &node);
#else
  int err_ret = syscall (SYS_getcpu, NULL, &node, NULL);
#endif
  if (err_ret)
    return err_ret;
  if (node > lock->max_node)
    {
      errno = EINVAL;
      return -1;
    }
  info->node = node;
  return err_ret;
}

/* Allocate a NUMA spinlock and return a pointer to it.  Caller should
   call free () on the NUMA spinlock pointer to free the memory when it
   is no longer needed.  */

struct numa_spinlock *
init_numa_spinlock (void)
{
  struct numa_node_info node_info = __get_numa_node_info ();
  unsigned int node_count = node_info.node_count;
  /* Allocate an array of struct numa_spinlock_node to hold info for all
     NUMA nodes with NUMA node number from getcpu () as index.  */
  unsigned int max_node = node_info.max_node;
  size_t size = (sizeof (struct numa_spinlock)
		 + (max_node + 1) * sizeof (struct numa_spinlock_node));
  struct numa_spinlock *lock;
  if (posix_memalign ((void **) &lock,
		      __alignof__ (struct numa_spinlock_node), size))
    return NULL;
  memset (lock, 0, size);
  lock->max_node = max_node;
  lock->node_count = node_count;
  return lock;
}
