/* Copyright (C) 2018 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#ifndef _NUMA_SPINLOCK_H
#define _NUMA_SPINLOCK_H

#include <features.h>

__BEGIN_DECLS

struct numa_spinlock_info;

/* The local NUMA spinlock for each node.  */
struct numa_spinlock_node
{
  /* List of threads who are spinning for the local NUMA lock in this
     node.  */
  struct numa_spinlock_info *head;
  char __pad[64 - sizeof (void *)];
} __attribute__((aligned (64)));

/* The global NUMA spinlock.  */
struct numa_spinlock
{
  /* List of threads who owns the global NUMA spinlock.  */
  struct numa_spinlock_info *owner;
  /* The maximium NUMA node number.  */
  unsigned int max_node;
  /* The number of NUMA nodes.  */
  unsigned int node_count;
  /* Arrays of lists of threads who are spinning for the local NUMA lock
     on NUMA nodes indexed by NUMA node number.  */
  struct numa_spinlock_node lists[];
};

/* The NUMA spinlock information for each thread.  */
struct numa_spinlock_info
{
  /* The next thread on the local NUMA spinlock thread list.  */
  struct numa_spinlock_info *next;
  /* The workload function of this thread.  */
  void *(*workload) (void *);
  /* The argument pointer passed to the workload function.  */
  void *argument;
  /* The return value of the workload function.  */
  void *result;
  /* The NUMA node number.  */
  unsigned int node;
  /* Non-zero to indicate that the thread wants the global NUMA
     spinlock.  */
  int pending;
  char __pad[64 - (4 * sizeof (void *) + 2 * sizeof (int))];
} __attribute__((aligned (64)));

extern struct numa_spinlock *init_numa_spinlock (void);
extern int init_numa_spinlock_info (struct numa_spinlock *,
				    struct numa_spinlock_info *);
extern void apply_numa_spinlock (struct numa_spinlock *,
				 struct numa_spinlock_info *);

__END_DECLS

#endif /* numa-spinlock.h */
